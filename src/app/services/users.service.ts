import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';


export interface getUsers {
  userid: number
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }
}
