import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  postProduct(data: any) {
    return this.http.post<any>("localhost:8080/product", data)
    .pipe(map((res:any)=> {
      return res;
    }))
  }

  getProduct() {
    return this.http.get<any>("localhost:8080/product")
    .pipe(map((res:any)=>{
      return res;

    }))
  }

  updateProduct(data: any, idproduct: number) {
    return this.http.put<any>("http://localhost:8080/product/"+idproduct,data)
    .pipe(map((res:any)=> {
      return res;
    }))
  }

  deleteProduct(idproduct: number) {
    return this.http.delete<any>("http://localhost:8080/product/" +idproduct)
    .pipe(map((res:any)=> {
      return res;
    }))
  }

}
