import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

export interface requestLogin {
  username: string
  password: string
}

interface responseLogin {
  access_token: string
}

interface requestRegister extends requestLogin {
  email: string
  name: string
}

interface responsesRegister extends requestRegister{
  userId: string
}

@Injectable()
export class AuthService {

  BASE_API: string = environment.base_api

  constructor(
    private http: HttpClient
  ) { }

  login(payload: requestLogin): Observable<responseLogin> {
    return this.http.post<responseLogin>(`${this.BASE_API}/login`, payload)
  }

  register(payload: requestRegister): Observable<responsesRegister> {
    return this.http.post<responsesRegister>(`${this.BASE_API}/register`, payload)
  }

}
