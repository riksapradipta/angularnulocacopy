import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-Manageusers',
  templateUrl: './Manageusers.component.html',
  styleUrls: ['./Manageusers.component.css']
})
export class ManageusersComponent implements OnInit {


  e = [
    {
      nama: 'Dzurrahman',
      umur: 23,
      status: false
    },
    {
      nama: 'Roki',
      umur: 26,
      status: true
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }
}
