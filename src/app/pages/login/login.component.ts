import { Component, OnInit } from '@angular/core';
import { AuthService, requestLogin } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username!: string;
  password!: string;

  constructor(
    private authService: AuthService, private router: Router
  ) { }

  ngOnInit(): void {
  }

  doLogin() {
    const payload: requestLogin  = {
      username: this.username ? this.username : "",
      password: this.password ? this.password : ""
    }

    console.log('payload', payload);
    this.authService.login(payload).subscribe(
      res => {
        console.log(res);
        localStorage.setItem('token', res.access_token)
        this.router.navigate(['/Manageproduct'])
      }, err => {
        console.error(err)
      })
  }

}
