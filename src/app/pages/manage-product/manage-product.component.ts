import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export class Product {
  constructor(
    public idproduct: number,
    public nameproduct: string,
    public idcategory: string

  ) {

  }

  }
@Component({
  selector: 'app-manage-product',
  templateUrl: './manage-product.component.html',
  styleUrls: ['./manage-product.component.css']
})
export class ManageProductComponent implements OnInit {
  Products: Product[];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.httpClient.get<any>('http://localhost:8080/product').subscribe(
      response => {
        console.log(response);
        this.Products = response;
      }
    );
  }

}
