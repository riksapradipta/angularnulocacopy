import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { InputComponent } from './input/input.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    CardComponent,
    InputComponent,
    NavbarComponent
  ],
  exports: [
    CardComponent,
    InputComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
